﻿using AutoMapper;
using GameStation.Mapper;
using GameStation.Models;
using GameStation.ViewModel;
using GameStation.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace GameStation.API
{
    public class GamePriceAPI
    {
        private RateAPI RateAPI;

        public GamePriceAPI(RateAPI rateAPI)
        {
            RateAPI = rateAPI;
        }
        
        private ProductListViewModel CallGameAPI(string term)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("https://www.pricecharting.com/");
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync("api/products?t=a0f93c46e0784e3c57b3398911be61e746d82d8f&q=" + term).Result;

            return JsonConvert.DeserializeObject<ProductListViewModel>(response.Content.ReadAsStringAsync().Result);
        }
      
        //Look in the API if the game exist and return matching names
        public List<string> GetGameName(string term)
        {
            char[] delimiter = { ',', '\'', ':', ';', '.' };

            ProductListViewModel ProductsList = CallGameAPI(term);
            /* Since the Games from the API doesn't have the same syntax as our DB, 
             * we need to remplace each 'special caractere' (delimiter[]) with nothing ''
             * and we need to make sure the strings aren't case sensitive.
             * this way, we take more games into consideration.
             */
             return  ProductsList.Products.Where(p => (
                    string.Join("", term.Trim().Split(delimiter)).Replace("  ", " ").IndexOf(string.Join("", p.product_name.Trim().Split(delimiter)).Replace("  ", " "), StringComparison.OrdinalIgnoreCase) >= 0
                || string.Join("", p.product_name.Trim().Split(delimiter)).Replace("  ", " ").IndexOf(string.Join("", term.Trim().Split(delimiter)).Replace("  ", " "), StringComparison.OrdinalIgnoreCase) >= 0
                )).Select(p => p.product_name).ToList();
        }

        //Retrieve game price with game name and console type
        public GamePriceViewModel GetPrices(Game game)
        {
            char[] delimiter = { ',', '\'', ':', ';', '.' };
            GamePriceViewModel gameViewModel = AutoMapperConfig.GamePriceMapper().Map<Game, GamePriceViewModel>(game);
            ProductListViewModel ProductsList = CallGameAPI(game.Name);

            /* Since the Games from the API doesn't have the same syntax as our DB, 
             * we need to remplace each 'special caractere' (delimiter[]) with nothing ''
             * and we need to make sure the strings aren't case sensitive.
             * this way, we take more games into consideration.
             */
            ProductViewModel product = ProductsList.Products.Where(p =>
                p.console_name.Contains(game.Console.Name) && (
                    string.Join("", game.Name.Trim().Split(delimiter)).Replace("  ", " ").IndexOf(string.Join("", p.product_name.Trim().Split(delimiter)).Replace("  ", " "), StringComparison.OrdinalIgnoreCase) >= 0
                || string.Join("", p.product_name.Trim().Split(delimiter)).Replace("  ", " ").IndexOf(string.Join("", game.Name.Trim().Split(delimiter)).Replace("  ", " "), StringComparison.OrdinalIgnoreCase) >= 0
                )).FirstOrDefault();

            if (product == null)
            {
                gameViewModel.PriceCIB = 0;
                gameViewModel.PriceLoose = 0;
                gameViewModel.PriceNew = 0;
            }
            else
            {
                gameViewModel.PriceCIB = ConvertPrice(product.cib_price);
                gameViewModel.PriceLoose = ConvertPrice(product.loose_price);  
                gameViewModel.PriceNew = ConvertPrice(product.new_price);
            }
            return gameViewModel;
        }

        //the API's price is a string. format: 1453 (the last two number are cents)
        //We need to convert that into a double for later convertion
        private double ConvertPrice(string price)
        {
            return Math.Round(((Convert.ToDouble(price) / 100) * RateAPI.GetCADExchangeRates()), 2);
        }

        
        


    }
}