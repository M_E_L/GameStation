﻿using GameStation.ViewModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace GameStation.API
{
    public class RateAPI
    {
        public RateAPI()
        {

        }

        //Call an API and retrieve the rate between USD and CND
        public double GetCADExchangeRates()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("https://api.fixer.io/");
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync("latest?base=USD").Result;
            ExchangeRateViewModel exchangeRate = JsonConvert.DeserializeObject<ExchangeRateViewModel>(response.Content.ReadAsStringAsync().Result);
            return exchangeRate.rates.rate; //keep two number after the comma
        }
    }

    
}