﻿using GameStation.API;
using GameStation.Repository;
using GameStation.Services;
using SimpleInjector;
using SimpleInjector.Integration.Web;
using SimpleInjector.Integration.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GameStation.App_Start
{
    public class InjectionConfig
    {
        public static void SetInjection()
        {
            Container container = new Container();
            container.Options.DefaultScopedLifestyle = new WebRequestLifestyle();

            //service
            container.Register<AnswerService>(Lifestyle.Transient);
            container.Register<Favorite_GameService>(Lifestyle.Transient);
            container.Register<FavoriteService>(Lifestyle.Transient);
            container.Register<GameService>(Lifestyle.Transient);
            container.Register<QuestionService>(Lifestyle.Transient);
            container.Register<ReviewService>(Lifestyle.Transient);
            container.Register<UserService>(Lifestyle.Transient);

            //repository
            container.Register<AnswerRepository>(Lifestyle.Transient);
            container.Register<Favorite_GameRepository>(Lifestyle.Transient);
            container.Register<FavoriteRepository>(Lifestyle.Transient);
            container.Register<GameRepository>(Lifestyle.Transient);
            container.Register<QuestionRepository>(Lifestyle.Transient);
            container.Register<ReviewRepository>(Lifestyle.Transient);
            container.Register<UserRepository>(Lifestyle.Transient);

            //API
            container.Register<GamePriceAPI>(Lifestyle.Transient);
            container.Register<RateAPI>(Lifestyle.Transient);


            container.Verify();

            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));
        }
    }
}