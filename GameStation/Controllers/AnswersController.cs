﻿using GameStation.Filter;
using GameStation.Mapper;
using GameStation.Models;
using GameStation.Repository;
using GameStation.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace GameStation.Controllers
{
    public class AnswersController : Controller
    {
        private AnswerRepository AnswerRepository;
        private QuestionRepository QuestionRepository;

        public AnswersController(AnswerRepository answerRepository, QuestionRepository questionRepository)
        {
            AnswerRepository = answerRepository;
            QuestionRepository = questionRepository;
        }

        // GET: Answers
        public ActionResult Index()
        {
            return View(AnswerRepository.GetAllAnswersByQuestion());
        }

        #region Votes

        [AuthentificationFilter]
        public ActionResult Upvote(int? AnswerId, int? UserId, bool? isUpvote)
        {
            if (AnswerId == null || UserId == null || isUpvote == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Answer answer = AnswerRepository.GetAnswerByUser((int)AnswerId, (int)UserId);
            if (answer == null)
            {
                return HttpNotFound();
            }
            if (isUpvote == true)
                answer.Vote++;
            else
                answer.Vote--;
            AnswerRepository.UpdateAnswer(answer);
            return RedirectToAction("Details", "Questions", new { id = answer.QuestionId });
        }

        #endregion

        #region Detail

        // GET: Answers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Answer answer = AnswerRepository.GetAnswerById((int)id);
            if (answer == null)
            {
                return HttpNotFound();
            }
            AnswerViewModel answerViewModel = AutoMapperConfig.AnswerMapper().Map<Answer, AnswerViewModel>(answer);
            answerViewModel.QuestionTitle = answer.Question.Title;
            return View(answerViewModel);
        }

        #endregion

        #region Create
        // GET: Answers/Create
        [AuthentificationFilter]
        public ActionResult Create(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Question question = QuestionRepository.GetQuestionById((int)id);
            int UserId = (int)this.Session["UserID"];
            if (question == null)
            {
                return HttpNotFound();
            }
            this.Session["QuestionId"] = id; //we need this, because the QuestionId won't transfe to the method below. TODO Find a solution..
            return View();
        }

        // POST: Answers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "QuestionId,Vote,Description,UserId")] Answer answer)
        {
            answer.QuestionId = (int)this.Session["QuestionId"];
            this.Session["QuestionId"] = null; //we don't need the id anymore
            answer.UserId = (int)this.Session["UserID"];

            if (ModelState.IsValid)
            {
                AnswerRepository.AddAnswer(answer);
                return RedirectToAction("Details", "Questions", new { id = answer.QuestionId });
            }
            AnswerViewModel answerViewModel = AutoMapperConfig.AnswerMapper().Map<Answer, AnswerViewModel>(answer);
            answerViewModel.QuestionTitle = answer.Question.Title;
            return View(answerViewModel);
        }

        #endregion

        #region Edit

        // GET: Answers/Edit/5
        [AuthentificationFilter]
        public ActionResult Edit(int? AnswerId, int? UserId)
        {
            if (AnswerId == null || UserId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Answer answer = AnswerRepository.GetAnswerByUser((int)AnswerId, (int)UserId);
            if (answer == null || UserId != answer.UserId)
            {
                return HttpNotFound();
            }

            AnswerViewModel answerViewModel = AutoMapperConfig.AnswerMapper().Map<Answer, AnswerViewModel>(answer);
            answerViewModel.QuestionTitle = answer.Question.Title;
            return View(answerViewModel);
        }

        // POST: Answers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,QuestionId,Vote,Description,UserId")] Answer answer)
        {
            if (ModelState.IsValid)
            {
                AnswerRepository.UpdateAnswer(answer);
                return RedirectToAction("Details", "Questions", new { id = answer.QuestionId });
            }
            AnswerViewModel answerViewModel = AutoMapperConfig.AnswerMapper().Map<Answer, AnswerViewModel>(answer);
            answerViewModel.QuestionTitle = answer.Question.Title;
            return View(answerViewModel);
        }

        #endregion

        #region Delete

        // GET: Answers/Delete/5
        [AuthentificationFilter]
        public ActionResult Delete(int? AnswerId, int? UserId)
        {
            if (AnswerId == null || UserId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Answer answer = AnswerRepository.GetAnswerByUser((int)AnswerId, (int)UserId);
            if (answer == null || UserId != answer.UserId)
            {
                return HttpNotFound();
            }
            AnswerViewModel answerViewModel = AutoMapperConfig.AnswerMapper().Map<Answer, AnswerViewModel>(answer);
            answerViewModel.QuestionTitle = answer.Question.Title;
            return View(answerViewModel);
        }

        // POST: Reviews/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int AnswerId, int UserId)
        {
            Answer answer = AnswerRepository.GetAnswerByUser((int)AnswerId, (int)UserId);
            int questionId = answer.QuestionId;
            AnswerRepository.DeleteAnswer(answer);
            return RedirectToAction("Details", "Questions", new { id = questionId });
        }

        #endregion

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                AnswerRepository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}