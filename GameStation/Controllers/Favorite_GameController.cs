﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GameStation.Models;
using GameStation.Filter;
using GameStation.Repository;

namespace GameStation.Controllers
{
    public class Favorite_GameController : Controller
    {
        //private GameStationEntities db = new GameStationEntities();
        private Favorite_GameRepository Favorite_GameRepository;
        private FavoriteRepository FavoriteRepository;

        public Favorite_GameController(Favorite_GameRepository favorite_GameRepository, FavoriteRepository favoriteRepository)
        {
            Favorite_GameRepository = favorite_GameRepository;
            FavoriteRepository = favoriteRepository;
        }


        #region Create

        // GET: Favorite_Game/Create
        [AuthentificationFilter]
        public ActionResult Create(int gameId)
        {
            this.Session["GameId"] = gameId;
            int UserId = (int)this.Session["UserId"];
            ViewBag.FavoriteId = new SelectList(FavoriteRepository.GetFavoriteByUserId(UserId), "ID", "Label");
            return View();
        }

        // POST: Favorite_Game/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,FavoriteId,GameId")] Favorite_Game favorite_Game)
        {
            favorite_Game.GameId = (int)this.Session["GameId"];
            int UserId = (int) this.Session["UserId"];
            //check if the user have already add this game in the list
            if (Favorite_GameRepository.GetFavorite_GameById((int) favorite_Game.GameId, (int) favorite_Game.FavoriteId) != null)
            {
                ViewBag.message = "You already have added this game in this list";
                ViewBag.FavoriteId = new SelectList(FavoriteRepository.GetFavoriteByUserId(UserId), "ID", "Label", favorite_Game.FavoriteId);
                return View(favorite_Game);
            }
            if (ModelState.IsValid)
            {
                Favorite_GameRepository.AddFavorite_GameDB(favorite_Game);
                this.Session["GameId"] = null;
                return RedirectToAction("Details", "Games", new { id = favorite_Game.GameId });
            }

            ViewBag.FavoriteId = new SelectList(FavoriteRepository.GetFavoriteByUserId(UserId), "ID", "Label", favorite_Game.FavoriteId);
            return View(favorite_Game);
        }

        #endregion
        
        #region Delete
        [AuthentificationFilter]
        // GET: Favorite_Game/Delete/5
        public ActionResult Delete(int? FavoriteId, int? GameId)
        {
            if (FavoriteId == null || GameId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Favorite_Game favorite_Game = Favorite_GameRepository.GetFavorite_GameById((int) GameId, (int) FavoriteId);
            if (favorite_Game == null)
            {
                return HttpNotFound();
            }
            return View(favorite_Game);
        }

        // POST: Favorite_Game/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int FavoriteId, int GameId)
        {
            Favorite_Game favorite_Game = Favorite_GameRepository.GetFavorite_GameById((int)GameId, (int)FavoriteId);
            Favorite_GameRepository.DeleteFavorite_Game(favorite_Game);
            return RedirectToAction("Details", "Favorites", new { id = FavoriteId});
        }

#endregion

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                Favorite_GameRepository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
