﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GameStation.Models;
using AutoMapper;
using GameStation.ViewModel;
using GameStation.Mapper;
using GameStation.Filter;
using GameStation.Repository;

namespace GameStation.Controllers
{
    public class FavoritesController : Controller
    {
        private FavoriteRepository FavoriteRepository;
        private GameRepository GameRepository;
        public FavoritesController(FavoriteRepository favoriteRepository, GameRepository gameRepository)
        {
            FavoriteRepository = favoriteRepository;
            GameRepository = gameRepository;
        }
        
        // GET: Favorites
        public ActionResult Index()
        {
            int? UserId = (int?)this.Session["UserId"];
            if (UserId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            List<Favorite> favorites = FavoriteRepository.GetFavoriteByUserId((int)UserId);
            List<FavoriteViewModel> favoritesViewModel = new List<FavoriteViewModel>();

            foreach(var favorite in favorites)
            {
                FavoriteViewModel favoriteViewModel = AutoMapperConfig.FavoriteMapper().Map<Favorite, FavoriteViewModel>(favorite);
                favoritesViewModel.Add(favoriteViewModel);
            }

            return View(favoritesViewModel);
        }

        // GET: Favorites/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Favorite favorite = FavoriteRepository.GetFavoriteById((int) id);
            if (favorite == null)
            {
                return HttpNotFound();
            }
            FavoriteViewModel favoriteViewModel = AutoMapperConfig.FavoriteMapper().Map<Favorite, FavoriteViewModel>(favorite);
            //Add all the games related to a Favorite to a viewModel
            foreach (var fav in favorite.Favorite_Game)
            {
                Game game = GameRepository.GetGameById((int) fav.GameId);
                GameViewModel gameViewModel = AutoMapperConfig.GameMapper().Map<Game, GameViewModel>(game);
                favoriteViewModel.gamesViewModel.Add(gameViewModel);
            }
            return View(favoriteViewModel);
        }

        #region Create
        [AuthentificationFilter]
        public ActionResult CreateOrAddFavorite(int? gameId)
        {
            int UserId = (int)this.Session["UserID"];
            if (gameId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if ( FavoriteRepository.GetFavoriteByUserId(UserId) == null)
            {
               TempData["error"] = "Before adding a game to your favorite you need to have at least one list!";
               return RedirectToAction("Create");
            }
            return View();
        }

        // GET: Favorites/Create
        [AuthentificationFilter]
        public ActionResult Create()
        {
            if (TempData["error"] != null)
                ViewBag.message = TempData["error"].ToString();
            return View();
        }

        // POST: Favorites/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Label,UserId")] Favorite favorite)
        {
            if (ModelState.IsValid)
            {
                favorite.UserId = (int)this.Session["UserID"];
                FavoriteRepository.AddFavoriteDB(favorite);
                return RedirectToAction("Index", "Favorites");
            }
            FavoriteViewModel favoriteViewModel = AutoMapperConfig.FavoriteMapper().Map<Favorite, FavoriteViewModel>(favorite);

            return View(favoriteViewModel);
        }

        #endregion

        #region Edit

        [AuthentificationFilter]
        // GET: Favorites/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Favorite favorite = FavoriteRepository.GetFavoriteById((int) id);
            if (favorite == null)
            {
                return HttpNotFound();
            }
            FavoriteViewModel favoriteViewModel = AutoMapperConfig.FavoriteMapper().Map<Favorite, FavoriteViewModel>(favorite);

            return View(favoriteViewModel);
        }

        // POST: Favorites/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Label,UserId")] Favorite favorite)
        {
            if (ModelState.IsValid) 
            {
                favorite.UserId = (int) this.Session["UserID"];
                FavoriteRepository.UpdateFavoriteDB(favorite);
                return RedirectToAction("Index");
            }
            FavoriteViewModel favoriteViewModel = AutoMapperConfig.FavoriteMapper().Map<Favorite, FavoriteViewModel>(favorite);

            return View(favoriteViewModel);
        }

        #endregion

        #region Delete

        // GET: Favorites/Delete/5
        [AuthentificationFilter]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Favorite favorite = FavoriteRepository.GetFavoriteById((int) id);
            if (favorite == null || favorite.UserId != (int) this.Session["UserID"])
            {
                return HttpNotFound();
            }
            return View(favorite);
        }

        // POST: Favorites/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            FavoriteRepository.DeleteFavoriteDB(id);
            return RedirectToAction("Index");
        }

        #endregion

        [AuthentificationFilter]
        public ActionResult AddOrCreateFavorite(int? gameId)
        {
            int UserId = (int)this.Session["UserID"];
            
            //check if the user doesn't have a list of favorite, he will be redirect to create one
            if (FavoriteRepository.GetFavoriteByUserId(UserId).Count() == 0)
            {
                return RedirectToAction("Create", "Favorites");
            }
            if (gameId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //if the user already have favorites
            return RedirectToAction("Create", "Favorite_Game", new { gameId = gameId});
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                FavoriteRepository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
