﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GameStation.Models;
using AutoMapper;
using GameStation.ViewModel;
using GameStation.Repository;
using GameStation.Mapper;
using Mapster;
using GameStation.Filter;

namespace GameStation.Controllers
{
    public class GamesController : Controller
    {
        private GameRepository GameRepository;

        public GamesController(GameRepository gameRepository)
        {
            GameRepository = gameRepository;
        }

        // GET: Games
        public ActionResult Index()
        {
            var games = GameRepository.GetAllGames();
            List<GameListViewModel> gamesViewModel = new List<GameListViewModel>();
            foreach (var game in games)
            {
                GameListViewModel gameViewModel = AutoMapperConfig.GameListMapper().Map<Game, GameListViewModel>(game);
                gamesViewModel.Add(gameViewModel);
            }
            return View(gamesViewModel.ToList());
        }

        // GET: Games/Details/5
        public ActionResult Details(int? id)
        {
            if (TempData["error"] != null)
                ViewBag.message = TempData["error"].ToString();
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Game game = GameRepository.GetGameById((int)id);
            if (game == null)
            {
                return HttpNotFound();
            }
            GamePriceViewModel gamePriceViewModel = GameRepository.GetGamePrice(game);
            gamePriceViewModel.CreatorUserName = game.User.Username;
            return View(gamePriceViewModel);
        }


        #region Create
        
        // GET: Games/Create
        [AuthentificationFilter]
        public ActionResult Create()
        {
            ViewBag.ConsoleId = new SelectList(GameRepository.GetAllConsole(), "ID", "Name");
            ViewBag.GenreId = new SelectList(GameRepository.GetAllGenre(), "ID", "Name");
            return View();
        }

        // POST: Games/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,ConsoleId,Name,ReleaseDate,Rating,Company,GenreId,Image")] Game game)
        {
            GameViewModel gameViewModel = AutoMapperConfig.GameMapper().Map<Game, GameViewModel>(game);

            if (!ModelState.IsValid)
            {
                ViewBag.ConsoleId = new SelectList(GameRepository.GetAllConsole(), "ID", "Name", gameViewModel.Console.ID);
                ViewBag.GenreId = new SelectList(GameRepository.GetAllGenre(), "ID", "Name", gameViewModel.Genre.ID);
                return View(gameViewModel);
            }

            if (GameRepository.GetGameByName_ConsoleId(game.Name, (int)game.ConsoleId) != null)
            {
                ViewBag.ConsoleId = new SelectList(GameRepository.GetAllConsole(), "ID", "Name", game.ConsoleId);
                ViewBag.GenreId = new SelectList(GameRepository.GetAllGenre(), "ID", "Name", game.GenreId);
                ViewBag.message = "**" + game.Name + " already exist on our site.**";
                return View(gameViewModel);
            }
            game.PosterId = (int)this.Session["UserID"];
            GameRepository.AddGameDB(game);
            return RedirectToAction("Index");

        }

        #endregion


        #region Edit

        // GET: Games/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Game game = GameRepository.GetGameById((int)id);
            if (game == null)
            {
                return HttpNotFound();
            }
            if (this.Session["UserID"] == null || (int)this.Session["UserID"] != game.PosterId)
            {
                TempData["error"] = "You don't have the authorization to modify this game";
                return RedirectToAction("Details", "Games", new { id = game.ID });
            }
            GameViewModel gameViewModel = AutoMapperConfig.GameMapper().Map<Game, GameViewModel>(game);
            ViewBag.ConsoleId = new SelectList(GameRepository.GetAllConsole(), "ID", "Name", game.ConsoleId);
            ViewBag.GenreId = new SelectList(GameRepository.GetAllGenre(), "ID", "Name", game.GenreId);
            return View(gameViewModel);
        }

        // POST: Games/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,ConsoleId,Name,releaseDate,Rating,Company,GenreId,PosterId,Image")] Game game)
        {
            if (ModelState.IsValid)
            {
                game.PosterId = (int)this.Session["UserID"];
                GameRepository.UpdateGame(game);
                return RedirectToAction("Index");
            }
            GameViewModel gameViewModel = AutoMapperConfig.GameMapper().Map<Game, GameViewModel>(game);
            ViewBag.ConsoleId = new SelectList(GameRepository.GetAllConsole(), "ID", "Name", game.ConsoleId);
            ViewBag.GenreId = new SelectList(GameRepository.GetAllGenre(), "ID", "Name", game.GenreId);
            return View(gameViewModel);
        }

        #endregion


        #region Delete

        // GET: Games/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Game game = GameRepository.GetGameById((int)id);
            if (game == null)
            {
                return HttpNotFound();
            }

            if (game.User.ID != (int)this.Session["UserID"])
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }
            GameViewModel gameViewModel = AutoMapperConfig.GameMapper().Map<Game, GameViewModel>(game);
            gameViewModel.CreatorUserName = game.User.Username;
            return View(gameViewModel);
        }

        // POST: Games/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            GameRepository.DeleteGame((int)id);
            return RedirectToAction("Index");
        }
        #endregion


        #region AutoComplete
        
        //return the name of the game already added in the DB
        public JsonResult GetGameName(string term)
        {
            return Json(GameRepository.GetAllGameName(term), JsonRequestBehavior.AllowGet);
        }

        //return the names of the game from the API
        public JsonResult GetGameNameAPI(string term)
        {
            return Json(GameRepository.GetAPIGameName(term), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetGamesByName(string term)
        {
            List<GameListViewModel> gamesViewModel = new List<GameListViewModel>();
            foreach (var game in GameRepository.GetGameByName(term))
            {
                 GameListViewModel gameViewModel = AutoMapperConfig.GameMapper().Map<Game, GameListViewModel>(game);
                gamesViewModel.Add(gameViewModel);
            }
            return View("Index", gamesViewModel.ToList());
        }

        #endregion


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                GameRepository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
