﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GameStation.Models;
using GameStation.Filter;
using GameStation.Repository;
using GameStation.ViewModel;
using GameStation.Mapper;

namespace GameStation.Controllers
{
    public class QuestionsController : Controller
    {
        private QuestionRepository QuestionRepository;
        private GameRepository GameRepository;
        //private UserRepository UserRepository;

        public QuestionsController(QuestionRepository questionRepository, GameRepository gameRepository)
        {
            QuestionRepository = questionRepository;
            GameRepository = gameRepository;
        }

        // GET: Questions
        public ActionResult Index()
        {
            var questions = QuestionRepository.GetAllQuestions();
            List<QuestionViewModel> questionsViewModel = new List<QuestionViewModel>();
            foreach(var q in questions)
            {
                QuestionViewModel questionViewModel = AutoMapperConfig.QuestionMapper().Map<Question, QuestionViewModel>(q);
                questionsViewModel.Add(questionViewModel);
            }

            return View(questionsViewModel);
        }

        // GET: Questions/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Question question = QuestionRepository.GetQuestionById((int)id);
            if (question == null)
            {
                return HttpNotFound();
            }
            QuestionViewModel questionViewModel = AutoMapperConfig.QuestionMapper().Map<Question, QuestionViewModel>(question);
            questionViewModel.QuestionCreator = question.User.Username;
            return View(questionViewModel);
        }

        // GET: Questions/Create
        [AuthentificationFilter]
        public ActionResult Create()
        {
            ViewBag.GameId = new SelectList(GameRepository.GetAllGames(), "ID", "Name");
            return View();
        }

        // POST: Questions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Title,Description,UserId,GameId")] Question question)
        {
            question.UserId = (int)this.Session["UserID"];

            if (ModelState.IsValid)
            {
                QuestionRepository.AddQuestionDB(question);
                return RedirectToAction("Index");
            }

            ViewBag.GameId = new SelectList(GameRepository.GetAllGames(), "ID", "Name", question.GameId);
            QuestionViewModel questionViewModel = AutoMapperConfig.QuestionMapper().Map<Question, QuestionViewModel>(question);
            return View(questionViewModel);
        }

        // GET: Questions/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Question question = QuestionRepository.GetQuestionById((int)id);
            if (question == null)
            {
                return HttpNotFound();
            }
            QuestionViewModel questionViewModel = AutoMapperConfig.QuestionMapper().Map<Question, QuestionViewModel>(question);
            ViewBag.GameId = new SelectList(GameRepository.GetAllGames(), "ID", "Name", question.GameId);
            return View(questionViewModel);
        }

        // POST: Questions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Title,Description,UserId,GameId")] Question question)
        {
            if (ModelState.IsValid)
            {
                QuestionRepository.UpdateQuestionDB(question);
                return RedirectToAction("Index");
            }
            QuestionViewModel questionViewModel = AutoMapperConfig.QuestionMapper().Map<Question, QuestionViewModel>(question);
            ViewBag.GameId = new SelectList(GameRepository.GetAllGames(), "ID", "Name", question.GameId);
            return View(questionViewModel);
        }

        // GET: Questions/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Question question = QuestionRepository.GetQuestionById((int)id);
            if (question == null)
            {
                return HttpNotFound();
            }
            if (question.User.ID != (int)this.Session["UserID"])
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }
            QuestionViewModel questionViewModel = AutoMapperConfig.QuestionMapper().Map<Question, QuestionViewModel>(question);
            questionViewModel.QuestionCreator = question.User.Username;
            return View(questionViewModel);
        }

        // POST: Questions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            QuestionRepository.DeleteQuestion(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                QuestionRepository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
