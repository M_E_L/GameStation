﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GameStation.Models;
using GameStation.ViewModel;
using AutoMapper;
using GameStation.Mapper;
using GameStation.Filter;
using GameStation.Repository;

namespace GameStation.Controllers
{
    public class ReviewsController : Controller
    {
        private ReviewRepository ReviewRepository;
        private GameRepository GameRepository;

        public ReviewsController(ReviewRepository reviewRepository, GameRepository gameRepository)
        {
            ReviewRepository = reviewRepository;
            GameRepository = gameRepository;
        }

        // GET: Reviews
        public ActionResult Index()
        {
            return View(ReviewRepository.GetAllReviewsByGame());
        }

        #region Upvotes

        [AuthentificationFilter]
        public ActionResult Upvote(int? GameId, int? UserId, bool? isUpvote)
        {
            if (GameId == null || UserId == null || isUpvote == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Review review = ReviewRepository.GetReviewByGame_User((int) GameId, (int) UserId);
            if (review == null)
            {
                return HttpNotFound();
            }
            if (isUpvote == true)
                review.Vote++;
            else
                review.Vote--;
            ReviewRepository.UpdateReview(review);
            return RedirectToAction("Details", "Games", new { id = review.GameId });
        }

        #endregion

        #region Detail

        // GET: Reviews/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Review review = ReviewRepository.GetReviewById((int) id);
            if (review == null)
            {
                return HttpNotFound();
            }
            return View(review);
        }

        #endregion

        #region Create
        // GET: Reviews/Create
        [AuthentificationFilter]
        public ActionResult Create(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Game game = GameRepository.GetGameById((int) id);
            int UserId = (int)this.Session["UserID"];
            if (ReviewRepository.GetReviewByGame_User(game.ID, UserId) != null)
            {
                TempData["error"] = "You have already reviewed this game";
                return RedirectToAction("Details", "Games", new { id = game.ID });
            }
            if (game == null)
            {
                return HttpNotFound();
            }
            this.Session["GameId"] = id; //we need this to transfert the GameId to the method below
            return View();
        }

        // POST: Reviews/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Vote,isRecommanded,Description,GameId,UserId")] Review review)
        {
           review.GameId = (int)this.Session["GameId"];
           this.Session["GameId"] = null; //we don't need the id anymore
            review.UserId = (int)this.Session["UserID"];
            if (ModelState.IsValid)
            {
                ReviewRepository.AddReview(review);
                return RedirectToAction("Details", "Games", new { id = review.GameId});
            }
            ReviewViewModel reviewViewModel = AutoMapperConfig.ReviewMapper().Map<Review, ReviewViewModel>(review);

            return View(reviewViewModel);
        }

        #endregion

        #region Edit

        // GET: Reviews/Edit/5
        [AuthentificationFilter]
        public ActionResult Edit(int? GameId, int? UserId)
        {
            if (GameId == null || UserId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Review review = ReviewRepository.GetReviewByGame_User((int) GameId, (int) UserId);
            if (review == null || UserId != review.UserId)
            {
                return HttpNotFound();
            }
            ReviewViewModel reviewViewModel = AutoMapperConfig.ReviewMapper().Map<Review, ReviewViewModel>(review);
            return View(reviewViewModel);
        }

        // POST: Reviews/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Vote,isRecommanded,Description,GameId,UserId")] Review review)
        {
            if (ModelState.IsValid)
            {
                ReviewRepository.UpdateReview(review);
                return RedirectToAction("Details", "Games", new { id = review.GameId });
            }
            ReviewViewModel reviewViewModel = AutoMapperConfig.ReviewMapper().Map<Review, ReviewViewModel>(review);
            return View(reviewViewModel);
        }

        #endregion

        #region Delete

        // GET: Reviews/Delete/5
        [AuthentificationFilter]
        public ActionResult Delete(int? GameId, int? UserId)
        {
            if (GameId == null || UserId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Review review = ReviewRepository.GetReviewByGame_User((int)GameId, (int)UserId);
            if (review == null || UserId != review.UserId)
            {
                return HttpNotFound();
            }
            ReviewViewModel reviewViewModel = AutoMapperConfig.ReviewMapper().Map<Review, ReviewViewModel>(review);
            return View(reviewViewModel);
        }

        // POST: Reviews/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int GameId, int UserId)
        {
            Review review = ReviewRepository.GetReviewByGame_User((int)GameId, (int)UserId);
            ReviewRepository.DeleteReview(review);
            return RedirectToAction("Details", "Games", new { id= GameId});
        }

#endregion

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                ReviewRepository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
