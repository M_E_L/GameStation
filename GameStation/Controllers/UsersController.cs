﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GameStation.Models;
using GameStation.ViewModel;
using AutoMapper;
using GameStation.Mapper;
using GameStation.Filter;
using GameStation.Repository;

namespace GameStation.Controllers
{
    public class UsersController : Controller
    {
        private UserRepository UserRepository;

        public UsersController(UserRepository userRepository)
        {
            UserRepository = userRepository;
        }

        // GET: Users/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = UserRepository.GetUserById((int)id);
            UserViewModel userViewModel = AutoMapperConfig.UserMapper().Map<User, UserViewModel>(user);

            if (user == null)
            {
                return HttpNotFound();
            }
            return View(userViewModel);
        }

        #region Login/Logout/Signup

        // GET: Users/Create
        public ActionResult Create()
        {
            return View();
        }
        
        // POST: Users/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Username,Password,BirthDate,Email")] User user)
        {
            if (ModelState.IsValid)
            {
                UserRepository.AddUserDB(user);
                this.Session["UserID"] = user.ID;
                return RedirectToAction("Index", "Games", null);
            }
            return View(user);
        }

        public ActionResult Login()
        {
            if (TempData["error"] != null)
                ViewBag.message = TempData["error"].ToString();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login([Bind(Include = "Username,Password")] LoginViewModel userLogin)
        {
            if (ModelState.IsValid)
            {
                int? userId = UserRepository.isUserExist(userLogin);
                if (/*UserExist(userLogin) ||*/ userId != null)
                {
                    this.Session["UserID"] = userId;
                    return RedirectToAction("Index", "Games");
                }
            }
            ViewBag.Message = "**Email or password is incorrect**";
            return View(userLogin);
        }

        public ActionResult Logout(User user)
        {
            this.Session["UserID"] = null;
            return RedirectToAction("Index", "Games");
        }


        //Make sure the Username is not already taken
        public JsonResult IsUsernameExists(string UserName)
        {
            return Json(!UserRepository.IsUsernameExists(UserName), JsonRequestBehavior.AllowGet);
        }
        

        public JsonResult IsValidBirthDate(DateTime birthdate)
        {
            //check if the user have between 7 years old and 120 years old
            if (birthdate.Year < DateTime.Now.Year - 7 && (birthdate.Year > DateTime.Now.Year - 120))
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Edit

        // GET: Users/Edit/5
        [AuthentificationFilter]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = UserRepository.GetUserById((int)id);
            UserViewModel userViewModel = AutoMapperConfig.UserMapper().Map<User, UserViewModel>(user);
            if (user == null || (int) this.Session["UserID"] != user.ID)
            {
                return HttpNotFound();
            }
            return View(userViewModel);
        }
        
        // POST: Users/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Username,Password,BirthDate,Email")] User user)
        {
            if (ModelState.IsValid)
            {
                UserRepository.EditUserDB(user);
                return RedirectToAction("Details", "Users", new { id = user.ID});
            }
            return View(user);
        }

        #endregion

        #region Delete 

        // GET: Users/Delete/5
        [AuthentificationFilter]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = UserRepository.GetUserById((int) id);
            UserViewModel userViewModel = AutoMapperConfig.UserMapper().Map<User, UserViewModel>(user);

            if (user == null)
            {
                return HttpNotFound();
            }
            return View(userViewModel);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!UserRepository.DeleteUser(id))
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }
            this.Session["UserId"] = null; //to logout the user
            return RedirectToAction("Index", "Games");
        }
#endregion

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                UserRepository.Dispose();
            }
            base.Dispose(disposing);
        }


    }
}
