﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GameStation.Filter
{
    public class AuthentificationFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //if not signed in redirect to login
            if (filterContext.HttpContext.Session["UserID"] == null)
            {
                filterContext.Controller.TempData["error"] = "You need to be logged in ";
                filterContext.Result = new RedirectResult("/Users/Login");
                return;
            }
        }
    }
}