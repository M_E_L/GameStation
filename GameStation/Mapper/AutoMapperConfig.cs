﻿using AutoMapper;
using GameStation.Models;
using GameStation.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GameStation.Mapper
{
    public class AutoMapperConfig
    {

        //Mapper game to GamePriceViewModel
        public static IMapper GamePriceMapper()
        {
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<Game, GamePriceViewModel>();
            });
            return config.CreateMapper();
        }

        //Mapper User to UserViewModel
        public static IMapper UserMapper()
        {
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<User, UserViewModel>();
            });
            
            return config.CreateMapper();
        }

        //Mapper Favorite to FavoriteViewModel
        public static IMapper FavoriteMapper()
        {
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<Favorite, FavoriteViewModel>();
            });

            return config.CreateMapper();
        }

        //Mapper Game to GameViewModel
        public static IMapper GameMapper()
        {
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<Game, GameViewModel>();
            });

            return config.CreateMapper();
        }

        //Mapper Game to GameViewModel
        public static IMapper GameListMapper()
        {
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<Game, GameListViewModel>();
            });

            return config.CreateMapper();
        }

        //Mapper Review to ReviewViewModel
        public static IMapper ReviewMapper()
        {
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<Review, ReviewViewModel>();
            });
            return config.CreateMapper();
        }

        public static IMapper QuestionMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Question, QuestionViewModel>();
            });
            return config.CreateMapper();
        }

        public static IMapper AnswerMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Answer, AnswerViewModel>();
            });
            return config.CreateMapper();
        }
    }
}