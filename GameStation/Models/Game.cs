//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GameStation.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Game
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Game()
        {
            this.Favorite_Game = new HashSet<Favorite_Game>();
            this.Questions = new HashSet<Question>();
            this.Reviews = new HashSet<Review>();
        }
    
        public int ID { get; set; }
        public Nullable<int> ConsoleId { get; set; }
        public string Name { get; set; }
        public System.DateTime releaseDate { get; set; }
        public decimal Rating { get; set; }
        public string Company { get; set; }
        public Nullable<int> GenreId { get; set; }
        public Nullable<int> PosterId { get; set; }
        public string Image { get; set; }
    
        public virtual Console Console { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Favorite_Game> Favorite_Game { get; set; }
        public virtual Genre Genre { get; set; }
        public virtual User User { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Question> Questions { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Review> Reviews { get; set; }
    }
}
