﻿using GameStation.Models;
using GameStation.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GameStation.Repository
{
    public class AnswerRepository
    {
        private AnswerService AnswerService;
        public AnswerRepository(AnswerService answerService)
        {
            AnswerService = answerService;
        }

        public List<Answer> GetAllAnswersByQuestion()
        {
            return AnswerService.GetAllAnswersByQuestion();
        }

        public Answer GetAnswerByUser(int answerId, int userId)
        {
            return AnswerService.GetAnswerByUser(answerId, userId);
        }

        public void UpdateAnswer(Answer answer)
        {
            AnswerService.UpdateAnswer(answer);
        }

        public Answer GetAnswerById(int id)
        {
            return AnswerService.GetAnswerById(id);
        }

        public void AddAnswer(Answer answer)
        {
            AnswerService.AddAnswer(answer);
        }

        public void DeleteAnswer(Answer answer)
        {
            AnswerService.DeleteAnswer(answer);
        }

        public void Dispose()
        {
            AnswerService.Dispose();
        }

    }
}