﻿using GameStation.Models;
using GameStation.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GameStation.Repository
{
    public class FavoriteRepository
    {
        private FavoriteService FavoriteService;
        public FavoriteRepository(FavoriteService favoriteService)
        {
            FavoriteService = favoriteService;
        }

        public List<Favorite> GetFavoriteByUserId(int id)
        {
            return FavoriteService.GetFavoriteByUserId(id);
        }
        public Favorite GetFavoriteById(int id)
        {
            return FavoriteService.GetFavoriteById(id);
        }
        public void AddFavoriteDB(Favorite favorite)
        {
            FavoriteService.AddFavoriteDB(favorite);
        }
        public void UpdateFavoriteDB(Favorite favorite)
        {
            FavoriteService.UpdateFavoriteDB(favorite);
        }
        public void DeleteFavoriteDB(int id)
        {
            FavoriteService.DeleteFavoriteDB(id);
        }

        public void Dispose()
        {
            FavoriteService.Dispose();
        }

    }
}