﻿using GameStation.Models;
using GameStation.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GameStation.Repository
{
    public class Favorite_GameRepository
    {
        private Favorite_GameService Favorite_GameService;
        private FavoriteService FavoriteService;
        public Favorite_GameRepository(Favorite_GameService favorite_GameService, FavoriteService favoriteService)
        {
            Favorite_GameService = favorite_GameService;
            FavoriteService = favoriteService;
        }

        public Favorite_Game GetFavorite_GameById(int GameId, int FavoriteId)
        {
            return Favorite_GameService.GetFavorite_GameByIDs(GameId, FavoriteId);
        }
        public List<Favorite_Game> GetFavorite_GameByFavoriteId(int FavoriteId)
        {
            return Favorite_GameService.GetFavorite_GameByFavoriteId(FavoriteId);
        }
        public void AddFavorite_GameDB(Favorite_Game fav)
        {
            Favorite_GameService.AddFavorite_GameDB(fav);
        }

        public void DeleteFavorite_Game(Favorite_Game fav)
        {
            Favorite_GameService.DeleteFavorite_Game(fav);
        }

        public void Dispose()
        {
            Favorite_GameService.Dispose();
        }
    }
}