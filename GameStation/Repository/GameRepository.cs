﻿using GameStation.API;
using GameStation.Models;
using GameStation.Services;
using GameStation.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GameStation.Repository
{
    public class GameRepository
    {
        private GamePriceAPI GamePriceAPI;

        private GameService GameService;

        public GameRepository(GameService gameService, GamePriceAPI gamePriceAPI)
        {
            GameService = gameService;
            GamePriceAPI = gamePriceAPI;
        }
        
        public List<string> GetAPIGameName(string term)
        {
            return GamePriceAPI.GetGameName(term);
        }

        public List<string> GetAllGameName(string term)
        {
            return GameService.GetAllGameName(term);
        }

        public List<Game> GetGameByName(string term)
        {
            return GameService.GetGameByName(term);
        }

        public Game GetGameById(int id)
        {
            return GameService.GetGameById(id);
        }
        public List<Game> GetAllGames()
        {
            return GameService.GetAllGames(); 
        }

        public GamePriceViewModel GetGamePrice(Game game)
        {
            return GamePriceAPI.GetPrices(game);
        }

        public Game GetGameByName_ConsoleId(string name, int consoleId)
        {
            return GameService.GetGameByName_ConsoleId(name, consoleId);
        }
        public void AddGameDB(Game game)
        {
            GameService.AddGameDB(game);
        }
        public void UpdateGame(Game game)
        {
            GameService.UpdateGame(game);
        }
        public void DeleteGame(int id)
        {
            GameService.DeleteGame(id);
        }

        public List<Genre> GetAllGenre()
        {
            return GameService.GetAllGenre();
        }

        public List<Models.Console> GetAllConsole()
        {
            return GameService.GetAllConsole();
        }

        public void Dispose()
        {
            GameService.Dispose();
        }
    }
}