﻿using GameStation.Models;
using GameStation.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GameStation.Repository
{
    public class QuestionRepository
    {

        private QuestionService QuestionService;
        public QuestionRepository(QuestionService questionService)
        {
            QuestionService = questionService;
        }

        public List<Question> GetAllQuestions()
        {
            return QuestionService.GetAllQuestions();
        }

        public Question GetQuestionById(int id)
        {
            return QuestionService.GetQuestionById(id);
        }

        public void AddQuestionDB(Question question)
        {
            QuestionService.AddQuestionDB(question);
        }

        public void UpdateQuestionDB(Question question)
        {
            QuestionService.UpdateQuestionDB(question);
        }

        public void DeleteQuestion(int id)
        {
            QuestionService.DeleteQuestion(id);
        }

        public void Dispose()
        {
            QuestionService.Dispose();
        }
    }
}