﻿using GameStation.Models;
using GameStation.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GameStation.Repository
{
    public class ReviewRepository
    {
        private ReviewService ReviewService;
        public ReviewRepository(ReviewService reviewService)
        {
            ReviewService = reviewService;
        }

        public List<Review> GetAllReviewsByGame()
        {
            return ReviewService.GetAllReviewsByGame();
        }

        public Review GetReviewByGame_User(int gameId, int userId)
        {
            return ReviewService.GetReviewByGame_User(gameId, userId);
        }
        public void UpdateReview(Review review)
        {
            ReviewService.UpdateReview(review);
        }

        public Review GetReviewById(int id)
        {
            return ReviewService.GetReviewById(id);
        }
        public void AddReview(Review review)
        {
            ReviewService.AddReview(review);
        }

        public void DeleteReview(Review review)
        {
            ReviewService.DeleteReview(review);
        }

        public void Dispose()
        {
            ReviewService.Dispose();
        }

    }
}