﻿using GameStation.Models;
using GameStation.Services;
using GameStation.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GameStation.Repository
{
    public class UserRepository
    {
        private UserService UserService;
        public UserRepository(UserService userService)
        {
            UserService = userService;
        }

        public User GetUserById(int id)
        {
            return UserService.GetUserById(id);
        }

        public void AddUserDB(User user)
        {
            UserService.AddUserDB(user);
        }

        public void EditUserDB(User user)
        {
            UserService.EditUserDB(user);
        }

        public int? isUserExist(LoginViewModel userLogin)
        {
            return UserService.isUserExist(userLogin);
        }

        public bool IsUsernameExists(string username)
        {
            return UserService.IsUsernameExists(username);
        }
        public bool DeleteUser(int id)
        {
            return UserService.DeleteUser(id);
        }

        public void Dispose()
        {
            UserService.Dispose();
        }
    }
}