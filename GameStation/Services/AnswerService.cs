﻿using GameStation.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GameStation.Services
{
    public class AnswerService
    {
        private GameStationEntities db = new GameStationEntities();

        public void DeleteAnswerByUserId(int UserId)
        {
            foreach (var answ in db.Answers.Where(a => a.UserId == UserId))
            {
                db.Answers.Remove(answ);
            }
            db.SaveChanges();
        }



        public List<Answer> GetAllAnswersByQuestion()
        {
            var answers = (from a in db.Answers
                          join q in db.Questions on a.QuestionId equals q.ID
                          join u in db.Users on a.UserId equals u.ID
                          select a).OrderBy(a => a.Vote);

            return answers.ToList();
        }

        public Answer GetAnswerByUser(int answerId, int userId)
        {
            var answers = from a in db.Answers
                          where a.ID == answerId
                          where a.UserId == userId
                          select a;

            return answers.FirstOrDefault();
        }

        public void UpdateAnswer(Answer answer)
        {
            db.Entry(answer).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public Answer GetAnswerById(int id)
        {
            return db.Answers.Find(id);
        }

        public void AddAnswer(Answer answer)
        {
            db.Answers.Add(answer);
            db.SaveChanges();
        }

        public void DeleteAnswer(Answer answer)
        {
            db.Answers.Remove(answer);
            db.SaveChanges();
        }

        public void Dispose()
        {
            db.Dispose();
        }
    }
}