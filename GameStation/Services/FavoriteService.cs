﻿using GameStation.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace GameStation.Services
{
    public class FavoriteService
    {
        private GameStationEntities db = new GameStationEntities();

        public List<Favorite> GetFavoriteByUserId(int id)
        {
            return db.Favorites.Where(f => f.UserId == id).ToList();
        }

        public Favorite GetFavoriteById(int id)
        {
            return db.Favorites.Find(id);
        }

        public void AddFavoriteDB(Favorite favorite)
        {
            db.Favorites.Add(favorite);
            db.SaveChanges();
        }

        public void UpdateFavoriteDB(Favorite favorite)
        {
            db.Entry(favorite).State = EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteFavoriteDB(int id)
        {
            Favorite favorite = GetFavoriteById(id);
            foreach (var fav_game in db.Favorite_Game.Where(f => f.FavoriteId == favorite.ID))
            {
                db.Favorite_Game.Remove(fav_game);
            }
            db.Favorites.Remove(favorite);
            db.SaveChanges();
        }

        public void DeleteFavoriteByUserId(int UserId)
        {
            foreach (var fav in db.Favorites.Where(f => f.UserId == UserId))
            {
                db.Favorites.Remove(fav);
            }
            db.SaveChanges();
        }

        public void Dispose()
        {
            db.Dispose();
        }
    }
}