﻿using GameStation.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GameStation.Services
{
    public class Favorite_GameService
    {
        private GameStationEntities db = new GameStationEntities();

        public Favorite_Game GetFavorite_GameByIDs(int GameId, int FavoriteId)
        {
            return db.Favorite_Game.Where(f => f.GameId == GameId && f.FavoriteId == FavoriteId).FirstOrDefault();
        }
        public List<Favorite_Game> GetFavorite_GameByFavoriteId(int FavoriteId)
        {
            return db.Favorite_Game.Where(f => f.FavoriteId == FavoriteId).ToList();
        }
        public void AddFavorite_GameDB(Favorite_Game favorite_Game)
        {
            db.Favorite_Game.Add(favorite_Game);
            db.SaveChanges();
        }
        public void DeleteFavorite_Game(Favorite_Game favorite_Game)
        {
            db.Favorite_Game.Remove(favorite_Game);
            db.SaveChanges();
        }
        public void Dispose()
        {
            db.Dispose();
        }

        public void DeleteFavorite_GameByUserId(int UserId)
        {
            foreach (var fav_game in db.Favorite_Game.Where(f => f.Favorite.UserId == UserId))
            {
                db.Favorite_Game.Remove(fav_game);
            }
            db.SaveChanges();
        }
    }
}