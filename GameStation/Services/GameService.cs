﻿using GameStation.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace GameStation.Services
{
    public class GameService
    {
        private GameStationEntities db = new GameStationEntities();

        public Game GetGameById(int id)
        {
            return db.Games.Find(id);
        }
        
        public List<Game> GetGameByName(string term)
        {
            return db.Games.Where(g => g.Name.Contains(term)).ToList();
        }
        public List<string> GetAllGameName(string term)
        {
            return db.Games.Where(g => g.Name.Contains(term)).Select(game => game.Name).ToList();
        }
        public List<Game> GetAllGames()
        {
            return db.Games.ToList(); //Include(g => g.Console).Include(g => g.Genre).Include(g => g.User)
        }
        public Game GetGameByName_ConsoleId(string name, int consoleId)
        {
            return db.Games.Where(g => g.Name.Equals(name) && g.ConsoleId == consoleId).FirstOrDefault();
        }
        public void AddGameDB(Game game)
        {
            db.Games.Add(game);
            db.SaveChanges();
        }

        public void UpdateGame(Game game)
        {
            db.Entry(game).State = EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteGame(int id)
        {
            Game game = db.Games.Find(id);
            db.Games.Remove(game);
            db.SaveChanges();
        }

        public List<Genre> GetAllGenre()
        {
            return db.Genres.ToList();
        }

        public List<Models.Console> GetAllConsole()
        {
            return db.Consoles.ToList();
        }

        public void Dispose()
        {
            db.Dispose();
        }

        public void DeleteGameByUserId(int UserId)
        {
            foreach (var game in db.Games.Where(g => g.PosterId == UserId))
            {
                db.Games.Remove(game);
            }
            db.SaveChanges();
        }
    }
}