﻿using GameStation.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace GameStation.Services
{
    public class QuestionService
    {
        private GameStationEntities db = new GameStationEntities();

        public void DeleteQuestionByUserId(int UserId)
        {
            foreach (var question in db.Questions.Where(q => q.UserId == UserId))
            {
                foreach (var answ in db.Answers.Where(a => a.QuestionId == question.ID))
                {
                    db.Answers.Remove(answ);
                }
                db.Questions.Remove(question);
            }
            db.SaveChanges();
        }

        public List<Question> GetAllQuestions()
        {
            var questions = db.Questions.Include(q => q.Game).Include(q => q.User);

            return questions.ToList();
        }

        public Question GetQuestionById(int id)
        {
            var question = from q in db.Questions
                           where q.ID == id
                           select q;

            return question.ToList().FirstOrDefault();
        }

        public void AddQuestionDB(Question question)
        {
            db.Questions.Add(question);
            db.SaveChanges();
        }

        public void UpdateQuestionDB(Question question)
        {
            db.Entry(question).State = EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteQuestion(int id)
        {
            Question question = db.Questions.Find(id);
            db.Questions.Remove(question);
            db.SaveChanges();
        }

        public void Dispose()
        {
            db.Dispose();
        }
    }
}