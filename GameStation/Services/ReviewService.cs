﻿using GameStation.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace GameStation.Services
{
    public class ReviewService
    {
        private GameStationEntities db = new GameStationEntities();

        public List<Review> GetAllReviewsByGame()
        {
            return db.Reviews.Include(r => r.Game).Include(r => r.User).ToList();
        }

        public Review GetReviewByGame_User(int gameId, int userId)
        {
            return db.Reviews.Where(r => r.GameId == gameId && r.UserId == userId).FirstOrDefault();
        }

        public void UpdateReview(Review review)
        {
            db.Entry(review).State = EntityState.Modified;
            db.SaveChanges();
        }
        public Review GetReviewById(int id)
        {
            return db.Reviews.Find(id);
        }

        public void AddReview(Review review)
        {
            db.Reviews.Add(review);
            db.SaveChanges();
        }
        public void DeleteReview(Review review)
        {
            db.Reviews.Remove(review);
            db.SaveChanges();
        }
        public void DeleteReviewByUserId(int UserId)
        {
            foreach (var review in db.Reviews.Where(r => r.UserId == UserId))
            {
                db.Reviews.Remove(review);
            }
            db.SaveChanges();
        }

        public void Dispose()
        {
            db.Dispose();
        }
    }
}