﻿using GameStation.Models;
using GameStation.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace GameStation.Services
{
    public class UserService
    {
        private GameStationEntities db = new GameStationEntities();

        public User GetUserById(int id)
        {
            return db.Users.Find(id);
        }

        public void AddUserDB(User user)
        {
            db.Users.Add(user);
            db.SaveChanges();
        }

        public void EditUserDB(User user)
        {
            db.Entry(user).State = EntityState.Modified;
            db.SaveChanges();
        }

        public bool IsUsernameExists(string username)
        {
            return db.Users.Any(u => u.Username.Equals(username));
        }

        public int? isUserExist(LoginViewModel userLogin)
        {
            //the foreach is used because the lamda expression isn't case sensitive
            foreach (var user in db.Users)
            {
                if (user.Username == userLogin.Username && user.Password == userLogin.Password)
                {
                    return user.ID;
                }
            }
            return null;
        }
        public void Dispose()
        {
            db.Dispose();
        }

        public bool DeleteUser(int id)
        {
            User user = GetUserById((int)id);
            ReviewService reviewService = new ReviewService();
            Favorite_GameService favorite_GameService = new Favorite_GameService();
            FavoriteService favoriteService = new FavoriteService();
            AnswerService answerService = new AnswerService();
            QuestionService questionService = new QuestionService();
            GameService gameService = new GameService();

            try
            {
                reviewService.DeleteReviewByUserId(user.ID);
                favorite_GameService.DeleteFavorite_GameByUserId(user.ID);
                favoriteService.DeleteFavoriteByUserId(user.ID);
                answerService.DeleteAnswerByUserId(user.ID);
                questionService.DeleteQuestionByUserId(user.ID);
                gameService.DeleteGameByUserId(user.ID);

                db.Users.Remove(user);
                db.SaveChanges();
            }
            catch(Exception e)
            {
                return false;
            }
            return true;
        }
    }
}
 