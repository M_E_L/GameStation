﻿using GameStation.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GameStation.ViewModel
{
    public class AnswerViewModel
    {

        public int ID { get; set; }
        public int QuestionId { get; set; }
        public string QuestionTitle { get; set; }
        public int Vote { get; set; }
        [Required(ErrorMessageResourceType = typeof(ErrorMessage), ErrorMessageResourceName = "RequiredField")]
        public string Description { get; set; }
        public int UserId { get; set; }

        public AnswerViewModel()
        {

        }
    }
}