﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GameStation.ViewModel
{
    public class ConsoleViewModel
    {
        public int ID { get; set; }

        [Display(Name = "Console Name")]
        public string Name { get; set; }

        [Display(Name = "Company Name")]
        public string Company { get; set; }

        public ConsoleViewModel()
        {
                
        }
    }
}