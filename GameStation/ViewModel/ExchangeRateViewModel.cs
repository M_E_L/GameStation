﻿using GameStation.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GameStation.ViewModel
{
    public class ExchangeRateViewModel
    {
        [JsonProperty(PropertyName = "base")]
        public string currencyBase { get; set; }

        [JsonProperty(PropertyName = "date")]
        public string date { get; set; }

        [JsonProperty(PropertyName = "rates")]
        public RateViewModel rates { get; set; }

        public ExchangeRateViewModel()
        {

        }
    }
}