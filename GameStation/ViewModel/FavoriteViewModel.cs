﻿using GameStation.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GameStation.ViewModel
{
    public class FavoriteViewModel
    {
        public int ID { get; set; }

        [Required(ErrorMessageResourceType = typeof(ErrorMessage), ErrorMessageResourceName = "RequiredField")]
        [Display(Name="Name")]
        public string Label { get; set; }
        public List<GameViewModel> gamesViewModel  { get; set; }
        public int UserId { get; set; }

        public FavoriteViewModel()
        {
            this.gamesViewModel = new List<GameViewModel>();
        }

    }
}