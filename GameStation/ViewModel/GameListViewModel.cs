﻿using GameStation.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GameStation.ViewModel
{
    /*ViewModel when the resume of a game is shown : Views/Games/Index*/
    public class GameListViewModel
    {
        public int ID { get; set; }

        [Display(Name = "Game Name")]
        public string Name { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}")]
        [Display(Name = "Release Date")]
        public DateTime ReleaseDate { get; set; }

        [Display(Name = "Rating")]
        public double Rating { get; set; }

        [Display(Name = "Genre")]
        public GenreViewModel Genre { get; set; }

        public ConsoleViewModel Console { get; set; }

        public string Image { get; set; }

        public GameListViewModel()
        {

        }
    }
}