﻿using GameStation.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GameStation.ViewModel
{
    /*The ViewModel when the detail of the game are shown => Views/Games/Details*/
    public class GamePriceViewModel
    {
        public int ID { get; set; }
        public double PriceCIB { get; set; }
        public double PriceLoose { get; set; }
        public double PriceNew { get; set; }
        public string CreatorUserName { get; set; }
        public string Name { get; set; }
        public ConsoleViewModel Console { get; set; }
        public GenreViewModel Genre { get; set; }
        public double Rating { get; set; }

        [DataType(DataType.Date)]
        public DateTime ReleaseDate { get; set; }
        public string Company { get; set; }
        public string Image { get; set; }
        public List<Review> Reviews { get; set; }

        public GamePriceViewModel()
        {
            this.Reviews = new List<Review>();
        }
        
        
    }
}