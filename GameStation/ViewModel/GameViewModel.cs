﻿using GameStation.Models;
using GameStation.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GameStation.ViewModel
{
    /*ViewModel when creating a game : Views/Games/Create*/
    public class GameViewModel
    {
        public int ID { get; set; }
        [Required(ErrorMessageResourceType = typeof(ErrorMessage), ErrorMessageResourceName = "RequiredField")]
        [Display(Name="Posted By")]
        public string CreatorUserName { get; set; }

        [Required(ErrorMessageResourceType = typeof(ErrorMessage), ErrorMessageResourceName = "RequiredField")]
        [Display(Name = "Title")]
        public string Name { get; set; }

        [Required(ErrorMessageResourceType = typeof(ErrorMessage), ErrorMessageResourceName = "RequiredField")]
        public ConsoleViewModel Console { get; set; }

        [Required(ErrorMessageResourceType = typeof(ErrorMessage), ErrorMessageResourceName = "RequiredField")]
        public GenreViewModel Genre { get; set; }

        [Required(ErrorMessageResourceType = typeof(ErrorMessage), ErrorMessageResourceName = "RequiredField")]
        [StringLength(1)]
        public double Rating { get; set; }

        [Required(ErrorMessageResourceType = typeof(ErrorMessage), ErrorMessageResourceName = "RequiredField")]
        [DataType(DataType.Date)]
        [Display(Name = "Release Date")]
        public DateTime ReleaseDate { get; set; }
        public string Company { get; set; }

        [Required(ErrorMessageResourceType = typeof(ErrorMessage), ErrorMessageResourceName = "RequiredField")]
        [DataType(DataType.ImageUrl)]
        [StringLength(250, MinimumLength = 6, ErrorMessageResourceType = typeof(ErrorMessage), ErrorMessageResourceName = "StringMinMax")]
        public string Image { get; set; }
        
        public List<Review> Reviews { get; set; }

        public GameViewModel()
        {
            this.Reviews = new List<Review>();
        }

    }
}