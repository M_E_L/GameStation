﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GameStation.ViewModel
{
    public class GenreViewModel
    {
        public int ID { get; set; }
        [Display(Name = "Genre")]
        public string Name { get; set; }

        public GenreViewModel()
        {

        }
    }
}