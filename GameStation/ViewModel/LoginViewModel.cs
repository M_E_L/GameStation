﻿using GameStation.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GameStation.ViewModel
{
    public class LoginViewModel
    {

        public int ID { get; set; }
        [Required(ErrorMessageResourceType = typeof(ErrorMessage), ErrorMessageResourceName = "RequiredField")]
        public string Username { get; set; }

        [Required(ErrorMessageResourceType = typeof(ErrorMessage), ErrorMessageResourceName = "RequiredField")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public LoginViewModel()
        {

        }
    }
}