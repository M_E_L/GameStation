﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GameStation.ViewModels
{
    /*ViewModel for the GameAPI*/
    public class ProductListViewModel
    {
        public List<ProductViewModel> Products { get; set; }

        public ProductListViewModel()
        {

        }
    }
}