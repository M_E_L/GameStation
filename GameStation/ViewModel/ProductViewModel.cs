﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GameStation.ViewModels
{
    /*ViewModel fot the GameAPI*/
    public class ProductViewModel
    {
        [JsonProperty(PropertyName = "asin")]
        public string asin { get; set; }

        [JsonProperty(PropertyName = "cib-price")]
        public string cib_price { get; set; }

        [JsonProperty(PropertyName = "console-name")]
        public string console_name { get; set; }

        [JsonProperty(PropertyName = "epid")]
        public string epid { get; set; }

        [JsonProperty(PropertyName = "genre")]
        public string genre { get; set; }

        [JsonProperty(PropertyName = "id")]
        public string id { get; set; }

        [JsonProperty(PropertyName = "loose-price")]
        public string loose_price { get; set; }

        [JsonProperty(PropertyName = "new-price")]
        public string new_price { get; set; }

        [JsonProperty(PropertyName = "product-name")]
        public string product_name { get; set; }

        public ProductViewModel()
        {

        }

    }
}