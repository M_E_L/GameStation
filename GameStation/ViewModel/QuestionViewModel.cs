﻿using GameStation.Resources;
using GameStation.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace GameStation.ViewModel
{
    public class QuestionViewModel
    {

        public int ID { get; set; }
        [Required(ErrorMessageResourceType = typeof(ErrorMessage), ErrorMessageResourceName = "RequiredField")]
        public string Title { get; set; }
        [Required(ErrorMessageResourceType = typeof(ErrorMessage), ErrorMessageResourceName = "RequiredField")]
        public string Description { get; set; }
        public string QuestionCreator { get; set; }
        [Display(Name = "Name")]
        public int GameId { get; set; }
        public int UserId { get; set; }
        public Game Game { get; set; }
        public User User { get; set; }
        public List<Answer> Answers { get; set; }

        public QuestionViewModel()
        {
            this.Answers = new List<Answer>();
        }

    }
}