﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GameStation.ViewModels
{
    /*ViewModel for the PriceAPI*/
    public class RateViewModel
    {
        [JsonProperty(PropertyName = "CAD")]
        public double rate { get; set; }


        public RateViewModel()
        {

        }
    }
}