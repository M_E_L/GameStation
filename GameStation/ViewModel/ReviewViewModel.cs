﻿using GameStation.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GameStation.ViewModel
{
    /*ViewModel when a rating is shown : Views/Games/Details*/
    public class ReviewViewModel
    {
        public int ID { get; set; }

        [Required(ErrorMessageResourceType = typeof(ErrorMessage), ErrorMessageResourceName = "RequiredField")]
        public string Description { get; set; }

        public int GameId { get; set; }
        public string GameName { get; set; }
        public int UserId { get; set; }

        [Required(ErrorMessageResourceType = typeof(ErrorMessage), ErrorMessageResourceName = "RequiredField")]
        public bool isRecommanded { get; set; }
        public int Vote { get; set; }

        public ReviewViewModel()
        {

        }
    }
}