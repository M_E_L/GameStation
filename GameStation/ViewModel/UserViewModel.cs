﻿using GameStation.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace GameStation.ViewModel
{
    public class UserViewModel
    {
        public int ID { get; set; }

        [Required(ErrorMessageResourceType = typeof(ErrorMessage), ErrorMessageResourceName = "RequiredField")]
        [Remote("IsUsernameExists", "Users", ErrorMessageResourceType = typeof(ErrorMessage), ErrorMessageResourceName = "AlreadyTaken")]
        [StringLength(30, MinimumLength = 6, ErrorMessageResourceType = typeof(ErrorMessage), ErrorMessageResourceName = "StringMinMax")]
        [RegularExpression(@"^[a-zA-Z''-'0-9_]{1,40}$", ErrorMessageResourceType = typeof(ErrorMessage), ErrorMessageResourceName = "AcceptOnlyLettersAndNumbers")]
        public string Username { get; set; }

        [Required(ErrorMessageResourceType = typeof(ErrorMessage), ErrorMessageResourceName = "RequiredField")]
        [DataType(DataType.Password)]
        [MembershipPassword(ErrorMessageResourceType = typeof(ErrorMessage), ErrorMessageResourceName = "PasswordRequirement")]
        [StringLength(30, MinimumLength = 7, ErrorMessageResourceType = typeof(ErrorMessage), ErrorMessageResourceName = "StringMinMax")]
        public string Password { get; set; }

        [Required(ErrorMessageResourceType = typeof(ErrorMessage), ErrorMessageResourceName = "RequiredField")]
        [DataType(DataType.Password)]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessageResourceType = typeof(ErrorMessage), ErrorMessageResourceName = "Mismatch")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessageResourceType = typeof(ErrorMessage), ErrorMessageResourceName = "RequiredField")]
        [DataType(DataType.Date)]
        [Remote("IsValidBirthDate", "Users", ErrorMessageResourceType = typeof(ErrorMessage), ErrorMessageResourceName = "InvalidField")]
        // [Range(minimum: DateTime.Now.Year, maximum: DateTime.Now.Year - 100), ErrorMessage = "Value for {0} must be between {1} and {2}")]
        public DateTime BirthDate { get; set; }

        [Required(ErrorMessageResourceType = typeof(ErrorMessage), ErrorMessageResourceName = "RequiredField")]
        [EmailAddress(ErrorMessageResourceType = typeof(ErrorMessage), ErrorMessageResourceName = "InvalidField")]
        public string Email { get; set; }

        public UserViewModel()
        {

        }
    }
}